package cn.uncode.springcloud.utils.sensitive.util;


import cn.uncode.springcloud.utils.string.StringUtil;


public class SensitiveInfoUtil {

	/**
	 * 真实姓名脱敏(展示姓名第一个字,其余用*替换)
	 * @param fullName
	 * @return name
	 */
    public static String handleTrueName(String fullName) {
        if (StringUtil.isBlank(fullName)) {
            return "";
        }
        return StringUtil.rightPad(StringUtil.left(fullName, 1), StringUtil.length(fullName), "*");
    }

    /**
     * 身份证号码脱敏(展示身份证号码前3位和最后4位,其余用*替换)
     * @param idCardNo
     * @return cardNo
     */
    public static String handleIdCardNo(String idCardNo) {
        if (StringUtil.isBlank(idCardNo)) {
            return "";
        }
        return StringUtil.left(idCardNo,3).concat(StringUtil.removeStart(StringUtil.leftPad(StringUtil.right(idCardNo, 4), StringUtil.length(idCardNo), "*"),"***"));
    }

    /**
     * 手机号码脱敏(展示手机号码前3位和最后4位,其余用*替换)
     * @param phoneNo
     * @return 手机号
     */
    public static String handlePhoneNo(String phoneNo) {
        if (StringUtil.isBlank(phoneNo)) {
            return "";
        }
        return StringUtil.left(phoneNo, 3).concat(StringUtil.removeStart(StringUtil.leftPad(StringUtil.right(phoneNo, 4),StringUtil.length(phoneNo), "*"), "***"));
    }

    /**
     * 银行卡号脱敏(展示银行卡号前6位和最后4位,其余用*替换)
     * @param bankcardNo
     * @return bankcardNo
     */
    public static String handleBankcardNo(String bankcardNo) {
        if (StringUtil.isBlank(bankcardNo)) {
            return "";
        }
        return StringUtil.left(bankcardNo, 6).concat(StringUtil.removeStart(StringUtil.leftPad(StringUtil.right(bankcardNo, 4), StringUtil.length(bankcardNo), "*"), "******"));
    }

    /**
     * 其它(全部用*替换)
     * @param msg
     * @return msg
     */
    public static String handleOther(String msg) {
        if (StringUtil.isBlank(msg)) {
            return "";
        }
        return StringUtil.rightPad("", StringUtil.length(msg), "*");
    }
}
