﻿//@ sourceURL=cache.js
layui.config({
    base: '../../lib/' //指定 winui 路径
    , version: '1.0.0-beta'
}).extend({
    winui: 'winui/winui',
    window: 'winui/js/winui.window'
}).define(['table','form', 'jquery', 'winui', 'window', 'layer'], function (exports) {

    winui.renderColor();

    var table = layui.table, form = layui.form,
        $ = layui.$, tableId = 'tableid';
    //桌面显示提示消息的函数
    var msg = top.winui.window.msg;
    
    var selectData = {};
	//加载数据
	$.ajax({
		url : '/cache/regions',
		type : "get",
		dataType : "json",
		contentType : "application/json",
		async : false,
		success : function(json) {
			if (json.success) {
				selectData = json.data;
				console.log(selectData);//没啥用
				var strs = "";
				for ( var x in selectData) {
					strs += '<option value = "' + selectData[x].name + '">'
							+ selectData[x].name + '</option>'
				}
				$("#name").html(strs);
			} else {
				msg(json.message)
			}
		}
	});
	form.render();
	//添加select的监听事件
    form.on('select(name)', function(data) { //no是那个lay-filter的值
		var nos = $("#name").val();//获得选中的option的值
		console.log(nos);//没啥用
		
		//表格渲染
	    table.render({
	        id: tableId,
	        elem: '#cache',
	        url: '/cache/list?storeRegion=' + nos,
	        page: true,
	        limits: [10, 20, 30, 40, 50, 60, 70, 100],
	        limit: 50,
	        cols: [[
	            { field: 'id', type: 'checkbox'},
	            { field: 'key', title: 'Key', width: 250 },
	            { field: 'ttl', title: 'TTL(s)', width: 100, templet: '#ttl'},
	            { field: 'value', title: 'Value', width: 250 },
	            { field: 'region', title: 'Region', width: 100},
	            { title: '操作', fixed: 'right', align: 'center', toolbar: '#barMenu', width: 100 }
	        ]]
	    });
	    //监听工具条
	    table.on('tool(cachetable)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
	        var data = obj.data; //获得当前行数据
	        var layEvent = obj.event; //获得 lay-event 对应的值
	        var ids = '';   //选中的Id
	        $(data).each(function (index, item) {
	            ids += item.id + ',';
	        });
	        if (layEvent === 'del') { //删除
	            deleteMenu(ids, data);
	        }
	    });
		
	});
 
    //删除菜单
    function deleteMenu(ids, obj) {
        var mg = obj.key ? '确认删除缓存【' + obj.key + '】吗？' : '确认删除选中数据吗？';
        top.winui.window.confirm(mg, { icon: 3, title: '删除缓存' }, function (index) {
            layer.close(index);
            $.ajax({
                type: 'get',
                url: '/cache/delete',
                data: { "ids": ids, "storeRegion": obj.region },
                success: function (json) {
                	if (json.success) {
                        msg('修改成功', {
                            icon: 1,
                            time: 2000
                        });
                        //刷新表格
                        reloadTable();  //直接刷新表格
                    } else {
                        msg(json.message);
                    }
                },
                error: function (xml) {
                    layer.close(index);
                    msg("获取页面失败", {
                        icon: 2,
                        time: 2000
                    });
                    console.log(xml.responseText);
                }
            });
        });
    }
    //表格刷新
    function reloadTable() {
        table.reload(tableId, {});
    }
   
    //绑定工具栏删除按钮事件
    $('#deleteMenu').on('click', function () {
        var checkStatus = table.checkStatus(tableId);
        var checkCount = checkStatus.data.length;
        if (checkCount < 1) {
            msg('请选择一条数据', {
                time: 2000
            });
            return false;
        }
        var ids = '';
        var obj = {};
        $(checkStatus.data).each(function (index, item) {
            ids += item.id + ',';
            obj["region"] = item.region;
        });
        deleteMenu(ids, obj);
    });
    
    //绑定工具栏添加按钮事件
    $('#testMenu').on('click', function () {
        var content;
        var index = layer.load(1);
        $.ajax({
            type: 'get',
            url: '/cache/test',
            success: function (json) {
            	if (json.success) {
                    msg('修改成功', {
                        icon: 1,
                        time: 2000
                    });
                } else {
                    msg(json.message);
                }
            },
            error: function (xml) {
                layer.close(load);
                msg('操作失败', {
                    icon: 2,
                    time: 2000
                });
                console.error(xml.responseText);
            }
        });
    });
    
  //绑定工具栏添加按钮事件
    $('#queryMenu').on('click', function () {
        var content;
        var index = layer.load(1);
        $.ajax({
            type: 'get',
            url: 'rediskey.html',
            success: function (data) {
                layer.close(index);
                content = data;
                //从桌面打开
                top.winui.window.open({
                    id: 'redisKey',
                    type: 1,
                    title: '缓存查询',
                    content: content,
                    area: ['50vw', '70vh'],
                    offset: ['15vh', '25vw']
                });
            },
            error: function (xml) {
                layer.close(load);
                msg('操作失败', {
                    icon: 2,
                    time: 2000
                });
                console.error(xml.responseText);
            }
        });
    });
    
    //绑定工具栏刷新按钮事件
    $('#reloadTable').on('click', reloadTable);

    exports('cache', {});
});
