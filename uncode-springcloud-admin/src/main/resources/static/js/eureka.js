﻿//@ sourceURL=menulist.js
layui.config({
    base: '../../lib/' //指定 winui 路径
    , version: '1.0.0-beta'
}).extend({
    winui: 'winui/winui',
    window: 'winui/js/winui.window'
}).define(['table', 'jquery', 'winui', 'window', 'layer'], function (exports) {

    winui.renderColor();

    var table = layui.table,
        $ = layui.$, tableId = 'tableid';
    //桌面显示提示消息的函数
    var msg = top.winui.window.msg;
    //表格渲染
    table.render({
        id: tableId,
        elem: '#eureka',
        url: '/eureka/apps',
        //height: 'full-65', //自适应高度
        size: 'sm',   //表格尺寸，可选值sm lg
        skin: 'nob',   //边框风格，可选值line row nob
        even:true,  //隔行变色
        page: false,
        //limits: [10, 20, 30, 40, 50, 60, 70, 100],
        //limit: 10,
        cols: [[
            { field: 'id', type: 'checkbox' },
            { field: 'name', title: '名称', width: 200 },
            { field: 'canaryFlag', title: '是否灰度', width: 80, templet: '#isCanary' },
            { field: 'status', title: '状态', width: 80, templet: '#status' },
            { field: 'ipAddr', title: '地址', width: 120 },
            { field: 'port', title: '端口', width: 60 },
            { field: 'instanceId', title: '实例', width: 210, templet: function(res){return '<a href='+res.statusPageUrl+'>'+ res.instanceId +'</a>'}},
            { field: 'lastUpdatedTime', title: '最近更新时间', width: 150, templet: function(res){return winui.dateFormat(res.lastUpdatedTime)}},
            { title: '操作', fixed: 'right', align: 'center', toolbar: '#barMenu', width: 100 }
        ]]
    });
    //监听工具条
    table.on('tool(eureka_table)', function (obj) { //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
        var data = obj.data; //获得当前行数据
        var layEvent = obj.event; //获得 lay-event 对应的值
        var tr = obj.tr; //获得当前行 tr 的DOM对象
        var ids = '';   //选中的Id
        $(data).each(function (index, item) {
            ids += item.id + ',';
        });
        if (layEvent === 'edit') { //编辑
            openEditWindow(data);
        }
    });
    //打开编辑窗口
    function openEditWindow(rdata) {
        if (!rdata) return;
        var content;
        var index = layer.load(1);
        $.ajax({
            type: 'get',
            url: 'edit.html?name=' + rdata.name,
            success: function (data) {
                layer.close(index);
                content = data;
                //从桌面打开
                top.winui.window.open({
                    id: 'editMenu',
                    type: 1,
                    title: '编辑节点信息',
                    content: content,
                    params:rdata,
                    area: ['50vw', '70vh'],
                    offset: ['15vh', '25vw'],
                });
            },
            error: function (xml) {
                layer.close(index);
                msg("获取页面失败", {
                    icon: 2,
                    time: 2000
                });
                console.log(xml.responseText);
            }
        });
    }
    //表格刷新
    function reloadTable() {
        table.reload(tableId, {});
    }
    //绑定工具栏编辑按钮事件
    $('#editMenu').on('click', function () {
        var checkStatus = table.checkStatus(tableId);
        var checkCount = checkStatus.data.length;
        if (checkCount < 1) {
            msg('请选择一条数据', {
                time: 2000
            });
            return false;
        }
        if (checkCount > 1) {
            msg('只能选择一条数据', {
                time: 2000
            });
            return false;
        }
        openEditWindow(checkStatus.data[0]);
    });
    //绑定工具栏刷新按钮事件
    $('#reloadTable').on('click', reloadTable);

    exports('eureka', {});
});
